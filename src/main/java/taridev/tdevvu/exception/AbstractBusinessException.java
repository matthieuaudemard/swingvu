package taridev.tdevvu.exception;

public class AbstractBusinessException extends Exception {
    public AbstractBusinessException() { super(); }

    public AbstractBusinessException(Throwable cause) {
        super(cause);
    }
}
