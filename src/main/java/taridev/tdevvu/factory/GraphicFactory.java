package taridev.tdevvu.factory;

import taridev.tdevvu.vo.AbstractVo;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public class GraphicFactory {

    private GraphicFactory() {
        // Empeche la création d'une instance de GraphicFactory.
    }

    /**
     * Retourne la valeur de l'attribut de nom "propertyName" sur l'objet cible vo
     * @param vo objet associé
     * @param propertyName nom de la propriété
     * @return valeur de la propriété
     */
    private static Object getValue(final AbstractVo vo, final String propertyName) {
        String methodName = "get" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        Object value = null;
        try {
            Method method = vo.getClass().getMethod(methodName);
            value = method.invoke(vo);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Permet d'invoquer le setter de la propriété propertyName avec en paramètre value.
     * @param vo l'objet cible de l'invoke
     * @param propertyName la propriété à setter
     * @param value la valeur à setter
     */
    private static void setValue(AbstractVo vo, final String propertyName, final Object value) {
        String methodName = "set" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        try {
            Method method = vo.getClass().getMethod(methodName, value.getClass());
            method.invoke(vo, value);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crée un JTextfield affichant la valeur de la propriété "propertyName" de l'objet vo.
     * @param parent composant auquel appartient le Textfield
     * @param vo objet associé à l'écran contenant le TextField
     * @param propertyName nom de la propriété appartenant au "vo" auquel est assoicé le JTextField
     * @param size taille du textField
     * @param tooltip texte du tooltip à afficher
     * @return JTextfield affichant la valeur de la propriété "propertyName" de l'objet vo.
     */
    public static JTextField createTextField (Container parent, final AbstractVo vo, final String propertyName, int size, String tooltip) {
        JTextField textField = new JTextField((String) GraphicFactory.getValue(vo, propertyName), size);
        textField.setName(propertyName);
        textField.setToolTipText(tooltip);

        // Changement du vo associé lorsque le texte du composant est modifié
        textField.addCaretListener(e -> setValue(vo, propertyName, textField.getText()));

        // Changement du texte à la modification du vo associé
        mapVoToComponent(textField, propertyName, vo);
        if (parent != null) {
            parent.add(textField);
        }
        return textField;
    }

    /**
     * Crée un label qui affiche la valeur en paramètre
     * @param value la valeur à afficher dans le JLabel
     * @return JLabel
     */
    public static JLabel createLabel (Container parent, final String value) {
        JLabel label = new JLabel(value);
        if (parent != null) {
            parent.add(label);
        }
        return label;
    }

    /**
     * Crée un label associé à la propriété du vo en paramètre
     * @param parent composant auquel appartient le label
     * @param vo objet associé à l'écran contenant le label
     * @param propertyName nom de la propriété appartenant au "vo" auquel est assoicé le label
     * @return JLabel
     */
    public static JLabel createLabel (Container parent, final AbstractVo vo, final String propertyName) {
        Object value = GraphicFactory.getValue(vo, propertyName);
        JLabel label = createLabel(parent, (value == null) ? null : value.toString());

        // Changement du texte à la modification du vo associé
        mapVoToComponent(label, propertyName, vo);
        if (parent != null) {
            parent.add(label);
        }
        return label;
    }

    private static void mapVoToComponent (JComponent component, final String propertyName, final AbstractVo vo) {
        component.setName(propertyName);
        vo.addPropertyChangeListener(e -> {
            Object newValue = e.getNewValue();
            if (component instanceof JTextComponent
                    && e.getPropertyName().equals(propertyName)
                    && !Objects.equals(newValue, ((JTextComponent) component).getText())) {
                SwingUtilities.invokeLater(() -> ((JTextComponent) component).setText((String) newValue));
            }
            else if (component instanceof JLabel
                    && e.getPropertyName().equals(propertyName)
                    && !Objects.equals(newValue, ((JLabel) component).getText())) {
                SwingUtilities.invokeLater(() -> ((JLabel) component).setText(newValue.toString()));
            }
            else if (component instanceof JCheckBox
                && e.getPropertyName().equals(propertyName)
                && !Objects.equals(newValue, ((JCheckBox) component).getText())) {
                JCheckBox checkbox = (JCheckBox) component;
                SwingUtilities.invokeLater(() -> {
                    checkbox.setEnabled(newValue != null);
                    checkbox.setSelected(newValue == null ? false : (Boolean) newValue);
                });
            }
            else if (component instanceof JSlider
                && e.getPropertyName().equals(propertyName)) {
                JSlider slider = (JSlider) component;
                SwingUtilities.invokeLater(() -> slider.setValue((Integer) newValue));
            }

        });
    }

    /**
     *
     * @param parent composant auquel appartient le Textfield
     * @param vo objet associé à l'écran contenant le TextField
     * @param propertyName nom de la propriété appartenant au "vo" auquel est assoicé le JTextField
     * @param title titre du label attaché au TextField
     * @param size taille du textField
     * @param tooltip texte du tooltip à afficher
     * @return Composant composé d'un JLabel et d'un JTextField
     */
    public static JComponent createTextFieldWithLabel (Container parent, final AbstractVo vo, final String propertyName, String title, int size, String tooltip) {
        JPanel containerPanel = new JPanel(new GridLayout(1, 2));
        createLabel(containerPanel, title);
        createTextField(containerPanel, vo, propertyName, size, tooltip);
        if (parent != null) {
            parent.add(containerPanel);
        }
        return containerPanel;
    }

    /**
     *
     * @param label associé au CheckBox
     * @param vo objet associé à l'écran contenant le CheckBox
     * @param propertyName nom de la propriété appartenant au "vo" auquel est associé le CheckBox
     * @return JCheckBox affichant la valeur de la propriété "propertyName" de l'objet vo.
     */
    public static JCheckBox createCheckBox(Container parent, String label, final AbstractVo vo, final String propertyName, String tooltip) {
        Boolean value = (Boolean) GraphicFactory.getValue(vo, propertyName);
        JCheckBox checkBox = new JCheckBox(label, value == null ? false : value);
        checkBox.setEnabled(value != null);
        checkBox.setToolTipText(tooltip);
        checkBox.addItemListener(e -> setValue(vo, propertyName, e.getStateChange() == ItemEvent.SELECTED));
        mapVoToComponent(checkBox, propertyName, vo);
        if (parent != null) {
            parent.add(checkBox);
        }
        return checkBox;
    }

    public static JSlider createSlider(Container parent, final AbstractVo vo, final String propertyName) {
        JSlider slider = new JSlider();
        slider.setValue((Integer) getValue(vo, propertyName));
        slider.addChangeListener(e -> setValue(vo, propertyName, ((JSlider) e.getSource()).getValue()));
        slider.setName(propertyName);
        mapVoToComponent(slider, propertyName, vo);
        if (parent != null) {
            parent.add(slider);
        }
        return slider;
    }

    public static JButton createButton(Container parent, final String title) {
        JButton button = new JButton(title);

        if (parent != null) {
            parent.add(button);
        }

        return button;
    }
}
