package taridev.tdevvu.creator;


import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.*;

public class TableModelCreator {

    private TableModelCreator() {
        // Constructeur privé pour empécher la création TableModelCreator
    }

    public static <T> TableModel createTableModel(Class<T> beanClass, List<T> list, Map<String, String> displayNames)
                throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(beanClass);
        List<String> columns = new ArrayList<>();
        List<Method> getters = new ArrayList<>();
        List<Class> columnClasses = new ArrayList<>();
        Set<String> descriptors = (displayNames == null) ? Collections.emptySet() : displayNames.keySet();

        // Parcours des attributs du Bean
        for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
            String name = descriptor.getName();

            // Ne rien faire si le nom correspond à la classe ou si l'attribut ne fait pas partie des attributs à afficher
            if (name.equals("class") || (displayNames != null && !descriptors.contains(name))) {
                continue;
            }

            String displayName;

            // Construction du label de colonne par défaut
            if (displayNames == null) {
                name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
                String[] s = name.split("(?=\\p{Upper})");
                StringBuilder displayNameBuilder = new StringBuilder();

                for (String s1 : s) {
                    displayNameBuilder.append(s1).append(" ");
                }

                displayName = displayNameBuilder.toString();
            } else {
                name = displayNames.get(name);
                displayName = name == null ? "" : name;
            }

            columns.add(displayName);
            getters.add(descriptor.getReadMethod());
            columnClasses.add(descriptor.getPropertyType());
        }

        return new AbstractTableModel() {
            @Override
            public String getColumnName(int column) {
                return columns.get(column);
            }

            @Override
            public Class getColumnClass(int column) {
                return columnClasses.get(column);
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return columns.size();
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                try {
                    return getters.get(columnIndex).invoke(list.get(rowIndex));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    public static <T> TableModel createTableModel(Class<T> beanClass, List<T> list) throws IntrospectionException {
        return TableModelCreator.createTableModel(beanClass, list, null);
    }


}
