package taridev.tdevvu.composite;

import taridev.tdevvu.vo.AbstractVo;

public abstract class AbstractCompositeHelper<T extends AbstractCompositeWithHelper> {

    private T composite;

    public AbstractCompositeHelper(T composite) {
        this.composite = composite;
    }

    protected T getComposite() {
        return composite;
    }

    protected AbstractVo getVO() {
        return composite.getVo();
    }
}
