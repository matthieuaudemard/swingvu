package taridev.tdevvu.composite;

import taridev.tdevvu.exception.AbstractBusinessException;
import taridev.tdevvu.vo.AbstractVo;

import javax.swing.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class AbstractCompositeWithHelper<T extends AbstractVo, H extends AbstractCompositeHelper> extends JPanel {

    private T model;
    private H helper;

    public AbstractCompositeWithHelper(T model) throws AbstractBusinessException {
        super();
        this.model = model;
        try {
            Class helperClass = Class.forName(getClass().getName().concat("Helper"));
            Constructor [] constructors = helperClass.getConstructors();
            this.helper = (H) constructors[0].newInstance(this);
            this.createContent();
        }
        catch (ClassNotFoundException
                | InstantiationException
                | IllegalAccessException
                | InvocationTargetException  e) {
            throw new AbstractBusinessException(e);
        }
    }

    public H getHelper() {
        return helper;
    }

    private void createContent() {
        this.innerCreateContent();
        this.mapComponentsToListeners();
    }

    public abstract void innerCreateContent();
    public abstract void mapComponentsToListeners();

    public T getVo() {
        return model;
    }

    public void setVo(final T vo) {
        this.model = vo;
    }
}
