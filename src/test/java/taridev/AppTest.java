package taridev;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import taridev.tdevvu.factory.SimpleGraphicFactoryGuiTest;

@RunWith( Suite.class )
@Suite.SuiteClasses( {SimpleGraphicFactoryGuiTest.class} )
public class AppTest 
{
}
