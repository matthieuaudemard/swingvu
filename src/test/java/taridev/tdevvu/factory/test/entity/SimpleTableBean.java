package taridev.tdevvu.factory.test.entity;

public class SimpleTableBean {
    private Integer anInteger;
    private String aString;
    private Boolean aBoolean;
    private Double aDouble;

    public SimpleTableBean() {
        // Constructeur par défaut
    }

    public SimpleTableBean(Integer anInteger, String aString, Boolean aBoolean, Double aDouble) {
        this.anInteger = anInteger;
        this.aString = aString;
        this.aBoolean = aBoolean;
        this.aDouble = aDouble;
    }

    public Integer getAnInteger() {
        return anInteger;
    }

    public void setAnInteger(Integer anInteger) {
        this.anInteger = anInteger;
    }

    public String getaString() {
        return aString;
    }

    public void setaString(String aString) {
        this.aString = aString;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }
}
