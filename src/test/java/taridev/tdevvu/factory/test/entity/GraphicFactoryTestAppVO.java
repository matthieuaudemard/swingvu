package taridev.tdevvu.factory.test.entity;

import taridev.tdevvu.vo.AbstractVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GraphicFactoryTestAppVO extends AbstractVo {
    private String textFieldValue;
    private Boolean checkBoxValue = Boolean.FALSE;
    private String textFieldWithLabelValue;
    private String labelValue;
    private String mappedLabelValue;
    private List<SimpleTableBean> tableList = new ArrayList<>();
    private Integer sliderValue = 0;

    public List<SimpleTableBean> getTableList() {
        return tableList;
    }

    public void setTableList(List<SimpleTableBean> tableList) {
        this.tableList = tableList;
    }


    public String getLabelValue() {
        return labelValue;
    }

    public void setLabelValue(String labelValue) {
        changeSupport.firePropertyChange("labelValue", this.labelValue, labelValue);
        this.labelValue = labelValue;
    }

    public String getTextFieldWithLabelValue() {
        return textFieldWithLabelValue;
    }

    public void setTextFieldWithLabelValue(String textFieldWithLabelValue) {
        changeSupport.firePropertyChange("labelValue", this.textFieldWithLabelValue, textFieldWithLabelValue);
        this.textFieldWithLabelValue = textFieldWithLabelValue;
    }

    public Boolean getCheckBoxValue() {
        return checkBoxValue;
    }

    public void setCheckBoxValue(Boolean checkBoxValue) {
        if (! Objects.equals(checkBoxValue, this.checkBoxValue)) {
            changeSupport.firePropertyChange("checkBoxValue", this.checkBoxValue, checkBoxValue);
            this.checkBoxValue = checkBoxValue;
        }
    }

    public void setTextFieldValue(String str) {
        if (! Objects.equals(str, this.textFieldValue)) {
            changeSupport.firePropertyChange("textFieldValue", this.textFieldValue, str);
            this.textFieldValue = str;
        }
    }

    public String getTextFieldValue() {
        return this.textFieldValue;
    }

    public String getMappedLabelValue() {
        return mappedLabelValue;
    }

    public void setMappedLabelValue(String mappedLabelValue) {
        if (! Objects.equals(mappedLabelValue, this.mappedLabelValue)) {
            changeSupport.firePropertyChange("mappedLabelValue", this.mappedLabelValue, mappedLabelValue);
            this.mappedLabelValue = mappedLabelValue;
        }
    }

    public Integer getSliderValue() {
        return sliderValue;
    }

    public void setSliderValue(Integer sliderValue) {
        if (! Objects.equals(sliderValue, this.sliderValue)) {
            changeSupport.firePropertyChange("sliderValue", this.sliderValue, sliderValue);
            this.sliderValue = sliderValue;
            System.out.println(sliderValue);
        }
    }
}
