package taridev.tdevvu.factory.test;

import taridev.tdevvu.creator.TableModelCreator;
import taridev.tdevvu.factory.test.entity.GraphicFactoryTestAppVO;
import taridev.tdevvu.factory.test.entity.SimpleTableBean;
import taridev.tdevvu.vo.AbstractVo;
import taridev.tdevvu.factory.GraphicFactory;

import java.awt.*;
import java.beans.IntrospectionException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;
import javax.swing.table.TableModel;

public class GraphicFactoryTestApp extends JFrame {

    private AbstractVo vo = new GraphicFactoryTestAppVO();

    public AbstractVo getVo() {
        return vo;
    }

    public GraphicFactoryTestApp() throws HeadlessException {
        super("GraphicFactoryTestApp");
        setName("GraphicFactoryTestApp");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new GridLayout(2, 1));
        setSize(800, 600);
        JPanel topPanel = new JPanel(new FlowLayout());
        GraphicFactory.createTextField(topPanel, vo, "textFieldValue", 40, "test tooltip");
        GraphicFactory.createCheckBox(topPanel,"checkbox", vo, "checkBoxValue", "check");
        GraphicFactory.createTextFieldWithLabel(topPanel, vo, "textFieldWithLabelValue", "label", 10, null);
        GraphicFactory.createLabel(topPanel, vo, "mappedLabelValue");
        GraphicFactory.createSlider(topPanel, vo, "sliderValue");

        HashMap<String, String> map = new HashMap<>();
        JScrollPane bottomPanel = null;
        add(topPanel);
        map.put("aString", null);
        map.put("aBoolean", "Vrai / Faux");
        try {
            TableModel tableModel = TableModelCreator.createTableModel(
                SimpleTableBean.class,
                ((GraphicFactoryTestAppVO) getVo()).getTableList(),
                map
            );
            JTable table = new JTable(tableModel);
            bottomPanel = new JScrollPane(table);
            GraphicFactoryTestAppVO castedVo = (GraphicFactoryTestAppVO) getVo();
            SimpleTableBean[] beanArray = new SimpleTableBean[] {
                new SimpleTableBean(1, "test", true, 2.0),
                new SimpleTableBean(12, "test 2", false, 2.2),
            };
            List<SimpleTableBean> beanList = Arrays.asList(beanArray);
            castedVo.getTableList().addAll(beanList);
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        add(bottomPanel);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            GraphicFactoryTestApp app = new GraphicFactoryTestApp();
            app.setVisible(true);
        });
    }
}
