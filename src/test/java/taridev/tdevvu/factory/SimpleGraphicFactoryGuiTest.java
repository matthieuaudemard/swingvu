package taridev.tdevvu.factory;

import org.assertj.swing.assertions.Assertions;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import taridev.tdevvu.factory.test.GraphicFactoryTestApp;
import taridev.tdevvu.factory.test.entity.GraphicFactoryTestAppVO;
import taridev.tdevvu.factory.test.entity.SimpleTableBean;

import java.util.Arrays;
import java.util.List;

public class SimpleGraphicFactoryGuiTest {

    private FrameFixture window;
    private GraphicFactoryTestAppVO vo;

    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    @Before
    public void setUp() {
        GraphicFactoryTestApp frame = GuiActionRunner.execute(() -> new GraphicFactoryTestApp());
        vo = (GraphicFactoryTestAppVO) frame.getVo();
        SimpleTableBean[] beanArray = new SimpleTableBean[] {
            new SimpleTableBean(1, "test", true, 2.0),
            new SimpleTableBean(12, "test 2", false, 2.2),
        };
        List<SimpleTableBean> beanList = Arrays.asList(beanArray);
        vo.getTableList().addAll(beanList);

        window = new FrameFixture(frame);
        window.show();
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

    @Test
    public void testTextFieldEnterValue() {
        String newValue = "sdfg";
        String propertyName = "textFieldValue";
        // saisie de texte "sdfg"
        //  - la valeur du vo doit être "sdfg"
        window.textBox(propertyName).enterText(newValue);
        Assertions.assertThat(vo.getTextFieldValue()).isEqualTo(newValue);

        // suppression du texte
        //  - la valeur du vo doit être ""
        window.textBox(propertyName).deleteText();
        Assertions.assertThat(vo.getTextFieldValue()).isEmpty();
    }

    @Test
    public void testCheckBoxCheckUncheck() {
        // coche de la case
        // - la valeur du vo doit être à True
        window.checkBox().check(true);
        Assertions.assertThat(vo.getCheckBoxValue()).isTrue();

        // décoche de la case
        // - la valeur du vo doit être à False
        window.checkBox().check(false);
        Assertions.assertThat(vo.getCheckBoxValue()).isFalse();
    }

    @Test
    public void testCheckBoxVoModify() {
        // Passage du vo à null :
        //  - la case doit être grisée
        //  - la case doit être non-cochée
        vo.setCheckBoxValue(null);
        Assertions.assertThat(window.checkBox().target().isEnabled()).isFalse();
        Assertions.assertThat(window.checkBox().target().isSelected()).isFalse();

        // Passage du vo à true :
        //  - la case doit être non-grisée
        //  - la case doit être cochée
        vo.setCheckBoxValue(true);
        Assertions.assertThat(window.checkBox().target().isEnabled()).isTrue();
        Assertions.assertThat(window.checkBox().target().isSelected()).isTrue();

        // Passage du vo à true :
        //  - la case doit être non-grisée
        //  - la case doit être non-cochée
        vo.setCheckBoxValue(false);
        Assertions.assertThat(window.checkBox().target().isEnabled()).isTrue();
        Assertions.assertThat(window.checkBox().target().isSelected()).isFalse();
    }

    @Test
    public void testTextFielsVoModify() {
        String newValue = "test";
        String propertyName = "textFieldValue";

        // passage du vo à newValue
        //  - le texte du champs doit être à newValue
        vo.setTextFieldValue(newValue);
        Assertions.assertThat(window.textBox(propertyName).text()).isEqualTo(newValue);

        // passage du vo à null
        //  - le texte du champs doit être vide ("")
        vo.setTextFieldValue(null);
        Assertions.assertThat(window.textBox(propertyName).text()).isEmpty();
    }

    @Test
    public void testTextFieldWithLabelEnterValue() {
        String newValue = "rtyuio";
        String propertyName = "textFieldWithLabelValue";
        window.textBox(propertyName).enterText(newValue);
        Assertions.assertThat(vo.getTextFieldWithLabelValue()).isEqualTo(newValue);
        window.textBox(propertyName).deleteText();
        Assertions.assertThat(vo.getTextFieldWithLabelValue()).isEmpty();
    }

    @Test
    public void testMappedLabelValueVoModify() {
        String newValue = "rtyuio";
        String propertyName = "mappedLabelValue";
        vo.setMappedLabelValue(newValue);
        Assertions.assertThat(window.label(propertyName).text()).isEqualTo(newValue);
    }

    @Test
    public void testSliderChangeValue() {
        Integer newValue = 18;
        String propertyName = "sliderValue";
        window.slider(propertyName).slideTo(18);
        Assertions.assertThat(vo.getSliderValue()).isEqualTo(newValue);
    }

    @Test
    public void testSliderVoValueModify() {
        Integer newValue = 23;
        String propertyName = "sliderValue";
        vo.setSliderValue(newValue);
        Assertions.assertThat(window.slider(propertyName).target().getValue()).isEqualTo(newValue);
    }
}
